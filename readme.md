ProjectE and Magical Mod Pack by DonRedhorse.

A Modpack which focuses on Magic, Exploration, Building, Hunger and not so well known Mods.
OH, and there is quite a lack of Energy in it.

While you will have a Questbook to help you a little bit along you should also take a closer look at the mods themselves,
as the Questbook will not hold your hand or explain the full contents of a mod to you. 

This Modpack doesn’t normally require any energy like RF, Tesla etc (but there is a backup wheel if your require it),
and it has a custom progression (not really that hard and hopefully not grindy). 

So let’s see if you can live without most of the big standards out there. I’m looking forward to your builds.