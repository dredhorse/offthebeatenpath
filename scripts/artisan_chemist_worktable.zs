import mods.artisanworktables.builder.RecipeBuilder;
import mods.artisanworktables.builder.Copy;
import crafttweaker.item.IItemStack;

// Chemist Worktable

static ChemistRecipe as RecipeBuilder = RecipeBuilder.get("chemist");

// Low Tier Ores

createLowTierRecipe("minecraft:iron_block");
createLowTierRecipe("minecraft:iron_ingot_from_nuggets");
createLowTierRecipe("unidict:blockcopper_x1_shape.aaaaaaaaa");
createLowTierRecipe("embers:block_coppper");
createLowTierRecipe("unidict:ingotcopper_x1_shape.aaaaaaaaa");
createLowTierRecipe("unidict:blockaluminium_x1_shape.aaaaaaaaa");
createLowTierRecipe("unidict:ingotaluminium_x1_shape.aaaaaaaaa");
createLowTierRecipe("unidict:blockbronze_x1_shape.aaaaaaaaa");
createLowTierRecipe("unidict:ingotbronze_x1_shape.aaaaaaaaa");
createLowTierRecipe("unidict:blocktin_x1_shape.aaaaaaaaa");
createLowTierRecipe("unidict:ingottin_x1_shape.aaaaaaaaa");

// High Tier Ores

createHighTierRecipe("minecraft:gold_block");
createHighTierRecipe("minecraft:gold_ingot_from_nuggets");
createHighTierRecipe("unidict:blocklead_x1_shape.aaaaaaaaa");
createHighTierRecipe("unidict:ingotlead_x1_shape.aaaaaaaaa");
createHighTierRecipe("embers:ingotdownstone_block");
createHighTierRecipe("embers:ingotdownstone_ingot");
createHighTierRecipe("unidict:blocksilver_x1_shape.aaaaaaaaa");
createHighTierRecipe("unidict:ingotsilver_x1_shape.aaaaaaaaa");
createHighTierRecipe("unidict:blockelectrum_x1_shape.aaaaaaaaa");
createHighTierRecipe("unidict:ingotelectrum_x1_shape.aaaaaaaaa");
createHighTierRecipe("unidict:blocknickel_x1_shape.aaaaaaaaa");
createHighTierRecipe("unidict:ingotnickel_x1_shape.aaaaaaaaa");

// ############################################## Functions




function createRecipe(item as string, usage as int, lavaAmount as int) {
   ChemistRecipe
   .setCopy(
    	Copy.byName(item)
    )
    .addTool(<ore:artisansBurner>, usage)
    .setFluid(<liquid:lava> * lavaAmount)
    .create();
    recipes.removeByRecipeName(item);

}

function createLowTierRecipe(item as string) {
	var usage = 1;
    var lavaAmount = 10;

	createRecipe(item, usage, lavaAmount);
}


function createHighTierRecipe(item as string) {
	var usage = 10;
    var lavaAmount = 100;

	createRecipe(item, usage, lavaAmount);
}