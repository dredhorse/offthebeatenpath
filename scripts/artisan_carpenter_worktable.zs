import mods.artisanworktables.builder.RecipeBuilder;
import mods.artisanworktables.builder.Copy;
import crafttweaker.item.IItemStack;
import thaumcraft.aspect.CTAspectStack;

// Carpenter Worktable

static CarpenterRecipe as RecipeBuilder = RecipeBuilder.get("carpenter");
static motus as CTAspectStack = <aspect:motus>.setAmount(15);
static aqua as CTAspectStack = <aspect:aqua>.setAmount(10);
static terra as CTAspectStack = <aspect:terra>.setAmount(11);

// making Boats harder

createBoat(<minecraft:planks>, <minecraft:boat>);
createBoat(<minecraft:planks:1>, <minecraft:spruce_boat>);
createBoat(<minecraft:planks:2>, <minecraft:birch_boat>);
createBoat(<minecraft:planks:3>, <minecraft:jungle_boat>);
createBoat(<minecraft:planks:4>, <minecraft:acacia_boat>);
createBoat(<minecraft:planks:5>, <minecraft:dark_oak_boat>);

recipes.removeByRegex("minecraft:[a-zA-Z_]*boat");


function createBoat(wood as IItemStack, output as IItemStack) { 
 
CarpenterRecipe.setShaped([
    [null, null, null],
    [wood, null, wood],
    [wood, <ore:rafts>, wood]])
    .setMinimumTier(1)
    .addTool(<ore:artisansHandsaw>, 10)
    .setSecondaryIngredients([<ore:materialPressedwax>])
	.setFluid(<liquid:water> * 1000)
	.addOutput(output)
	.setExtraOutputOne(<thaumcraft:crystal_essence>.withTag({Aspects: [{amount: 1, key: "iter"}]}) * 5, 1)
    .create();
output.setAspects(motus, aqua, terra);
}
  