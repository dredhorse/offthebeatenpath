#priority 10000

// make an oredict entry for vis crystal
oreDict.visCrystal;
<ore:visCrystal>.add(<thaumcraft:crystal_essence>);


// make an oredict entry for rafts
oreDict.rafts;
<ore:rafts>.add(<jarm:oak_raft>);
<ore:rafts>.add(<jarm:spruce_raft>);
<ore:rafts>.add(<jarm:birch_raft>);
<ore:rafts>.add(<jarm:jungle_raft>);
<ore:rafts>.add(<jarm:acacia_raft>);
<ore:rafts>.add(<jarm:dark_oak_raft>);

// make an oredict entry for fancy workbenches
oreDict.fancyWorkbench;
<ore:fancyWorkbench>.add(<bibliocraft:fancyworkbench>);
<ore:fancyWorkbench>.add(<bibliocraft:fancyworkbench:1>);
<ore:fancyWorkbench>.add(<bibliocraft:fancyworkbench:2>);
<ore:fancyWorkbench>.add(<bibliocraft:fancyworkbench:3>);
<ore:fancyWorkbench>.add(<bibliocraft:fancyworkbench:4>);
<ore:fancyWorkbench>.add(<bibliocraft:fancyworkbench:6>);


// make an oredict entry for Reliquary Pedestals
oreDict.reliquaryPedestal;
<ore:reliquaryPedestal>.add(<xreliquary:pedestal>);
<ore:reliquaryPedestal>.add(<xreliquary:pedestal:1>);
<ore:reliquaryPedestal>.add(<xreliquary:pedestal:2>);
<ore:reliquaryPedestal>.add(<xreliquary:pedestal:3>);
<ore:reliquaryPedestal>.add(<xreliquary:pedestal:4>);
<ore:reliquaryPedestal>.add(<xreliquary:pedestal:5>);
<ore:reliquaryPedestal>.add(<xreliquary:pedestal:6>);
<ore:reliquaryPedestal>.add(<xreliquary:pedestal:7>);
<ore:reliquaryPedestal>.add(<xreliquary:pedestal:8>);
<ore:reliquaryPedestal>.add(<xreliquary:pedestal:9>);
<ore:reliquaryPedestal>.add(<xreliquary:pedestal:10>);
<ore:reliquaryPedestal>.add(<xreliquary:pedestal:11>);
<ore:reliquaryPedestal>.add(<xreliquary:pedestal:12>);
<ore:reliquaryPedestal>.add(<xreliquary:pedestal:13>);
<ore:reliquaryPedestal>.add(<xreliquary:pedestal:14>);
<ore:reliquaryPedestal>.add(<xreliquary:pedestal:15>);

// make an oredict entry for Dank Null
oreDict.dankNull;
<ore:dankNull>.add(<danknull:dank_null>);
<ore:dankNull>.add(<danknull:dank_null:1>);
<ore:dankNull>.add(<danknull:dank_null:2>);
<ore:dankNull>.add(<danknull:dank_null:3>);
<ore:dankNull>.add(<danknull:dank_null:4>);
<ore:dankNull>.add(<danknull:dank_null:5>);

// make an oredict entry for Tape Measures
oreDict.tapeMeasure;
<ore:tapeMeasure>.add(<bibliocraft:tapemeasure>);
<ore:tapeMeasure>.add(<chiselsandbits:tape_measure>);

// make an oredict for portable sound mufflers
oreDict.earPhones;
<ore:earPhones>.add(<randomthings:portablesounddampener>);
<ore:earPhones>.add(<supersoundmuffler:sound_muffler_bauble>);

// make an oredict for sinks

oreDict.sinks;
<ore:sinks>.add(<of:counter_sink>);
<ore:sinks>.add(<harvestcraft:well>);
<ore:sinks>.add(<cookingforblockheads:sink>);


// add vanilla minecraft redstone to oredict

<ore:repeater>.add(<minecraft:repeater>);
<ore:comparator>.add(<minecraft:comparator>);
<ore:dustColoredRedstone>.add(<minecraft:redstone>);

