#priority 10000

// Hide from JEI
import crafttweaker.item.IItemStack;


var Hide = [
	<refinedstorage:cover>,
    <refinedstorage:hollow_cover>,
    <artisanworktables:worktable:0>, // Tailor
    #<artisanworktables:worktable:1>, // Carpenter
    <artisanworktables:worktable:2>, // Mason
    <artisanworktables:worktable:4>, // Jeweller
    <artisanworktables:worktable:5>, // Basic
    <artisanworktables:worktable:8>, // Scribe
    #<artisanworktables:worktable:9>, // Chemist
    <artisanworktables:worktable:10>, // Farmer
    <artisanworktables:worktable:11>, // Chef
    <artisanworktables:workstation:0>, // Tailor
    #<artisanworktables:workstation:1>, // Carpenter
    <artisanworktables:workstation:2>, // Mason
    <artisanworktables:workstation:3>, // Blacksmith
    <artisanworktables:workstation:4>, // Jeweller
    <artisanworktables:workstation:5>, // Basic
    <artisanworktables:workstation:6>, // Engineer
    <artisanworktables:workstation:7>, // Mage
    <artisanworktables:workstation:8>, // Scribe
    <artisanworktables:workstation:9>, // Chemist
    <artisanworktables:workstation:10>, // Farmer
    <artisanworktables:workstation:11> // Chef
  
    
] as IItemStack[];


for item in Hide{
    mods.jei.JEI.hide(item);
}
