import crafttweaker.item.IItemTransformer;

// adding Recipe for Artisan Worktables

recipes.addShapeless(<artisanworktables:worktable:3>, [<ore:workbench>, <ore:artisansHammer>]);
recipes.addShapeless(<artisanworktables:worktable:7>, [<ore:workbench>, <ore:artisansGrimoire>]);
recipes.addShapeless(<artisanworktables:worktable:6>, [<ore:workbench>, <ore:artisansPliers>]);
recipes.addShapeless(<artisanworktables:worktable:1>, [<ore:workbench>, <ore:artisansHandsaw>]);
recipes.addShapeless(<artisanworktables:worktable:9>, [<ore:workbench>, <ore:artisansBurner>]);


// add back one refined storage cover as example
var rsStoneCover = <refinedstorage:cover>.withTag({Item: {id: "minecraft:stone", Count: 1 as byte, Damage: 0 as short}});
var rsStoneHollowCover = <refinedstorage:hollow_cover>.withTag({Item: {id: "minecraft:stone", Count: 1 as byte, Damage: 0 as short}});

recipes.addShaped(rsStoneCover * 6, [[<refinedstorage:cutting_tool>.anyDamage().transformDamage(1), <minecraft:stone>]]);
rsStoneCover.addTooltip(format.red("Most other blocks work the same way"));
recipes.addShaped(rsStoneHollowCover * 8, [[rsStoneCover, rsStoneCover, rsStoneCover], [rsStoneCover, null, rsStoneCover], [rsStoneCover, rsStoneCover, rsStoneCover]]);
rsStoneHollowCover.addTooltip(format.red("Most other blocks work the same way"));
<refinedstorage:cutting_tool>.addTooltip(format.red("Look at usage for Covers"));

// adding Recipe for ClayBucket Ink
recipes.addShaped(<minecraft:dye:0> * 2, [[<ceramics:clay_bucket>.withTag({fluids: {FluidName: "water", Amount: 1000}}), <dooglamoojuniorarchaeologymod:archaeology_item00>, null],[<dooglamoojuniorarchaeologymod:archaeology_item03>, <dooglamoojuniorarchaeologymod:archaeology_item03>, null], [null, null, null]]);


// add recipe for missing ClayBucket Vahosaline
recipes.addShaped(<ceramics:clay_bucket>.withTag({fluids: {FluidName: "vasholine", Amount: 1000}}), [[<tp:wub_gem>, <tp:wub_gem>, <tp:wub_gem>], [<tp:wub_gem>, <ceramics:clay_bucket>, <tp:wub_gem>], [<tp:wub_gem>, <tp:wub_gem>, <tp:wub_gem>]]);


// add exchange recipe for mutton
recipes.addShapeless(<minecraft:mutton>, [<animania:raw_prime_mutton>]);

// add back stone torches => realistic torches
recipes.addShapeless(<realistictorches:torch_unlit> * 4, [<minecraft:coal:1>, <tp:stone_stick>]);
recipes.addShapeless(<realistictorches:torch_unlit> * 4, [<minecraft:coal:0>, <tp:stone_stick>]);

// add quartz spider nest in furnace to quartz
furnace.addRecipe(<minecraft:quartz>, <dooglamoojuniorarchaeologymod:archaeology_item01>);

// cycle through milk from animania

val onlyMilk = <minecraft:milk_bucket>.transformConsume(1);

recipes.addShapeless( <forge:bucketfilled>.withTag({FluidName: "milk_jersey", Amount: 1000}),[onlyMilk]);
recipes.addShapeless( <forge:bucketfilled>.withTag({FluidName: "milk_friesian", Amount: 1000}),[onlyMilk]);
recipes.addShapeless( <forge:bucketfilled>.withTag({FluidName: "milk_holstein", Amount: 1000}),[onlyMilk]);
recipes.addShapeless( <forge:bucketfilled>.withTag({FluidName: "milk_goat", Amount: 1000}),[onlyMilk]);
recipes.addShapeless( <forge:bucketfilled>.withTag({FluidName: "milk_sheep", Amount: 1000}),[onlyMilk]);


// fast filling the breadcrumbs pouch

var breadcrumbPouchFull = <breadcrumbtrail:breadcrumb_pouch>.withTag({OpenBag: 0 as byte, BagContents: {Size: 8, Items: [{Slot: 0, id: "breadcrumbtrail:breadcrumb", Count: 64 as byte, Damage: 0 as short}, {Slot: 1, id: "breadcrumbtrail:breadcrumb", Count: 64 as byte, Damage: 0 as short}, {Slot: 2, id: "breadcrumbtrail:breadcrumb", Count: 64 as byte, Damage: 0 as short}, {Slot: 3, id: "breadcrumbtrail:breadcrumb", Count: 64 as byte, Damage: 0 as short}, {Slot: 4, id: "breadcrumbtrail:breadcrumb", Count: 64 as byte, Damage: 0 as short}, {Slot: 5, id: "breadcrumbtrail:breadcrumb", Count: 64 as byte, Damage: 0 as short}, {Slot: 6, id: "breadcrumbtrail:breadcrumb", Count: 64 as byte, Damage: 0 as short}, {Slot: 7, id: "breadcrumbtrail:breadcrumb", Count: 64 as byte, Damage: 0 as short}]}});
var glowingBreadcrumbPouchFull = <breadcrumbtrail:breadcrumb_pouch>.withTag({OpenBag: 0 as byte, BagContents: {Size: 8, Items: [{Slot: 0, id: "breadcrumbtrail:breadcrumb_glowing", Count: 64 as byte, Damage: 0 as short}, {Slot: 1, id: "breadcrumbtrail:breadcrumb_glowing", Count: 64 as byte, Damage: 0 as short}, {Slot: 2, id: "breadcrumbtrail:breadcrumb_glowing", Count: 64 as byte, Damage: 0 as short}, {Slot: 3, id: "breadcrumbtrail:breadcrumb_glowing", Count: 64 as byte, Damage: 0 as short}, {Slot: 4, id: "breadcrumbtrail:breadcrumb_glowing", Count: 64 as byte, Damage: 0 as short}, {Slot: 5, id: "breadcrumbtrail:breadcrumb_glowing", Count: 64 as byte, Damage: 0 as short}, {Slot: 6, id: "breadcrumbtrail:breadcrumb_glowing", Count: 64 as byte, Damage: 0 as short}, {Slot: 7, id: "breadcrumbtrail:breadcrumb_glowing", Count: 64 as byte, Damage: 0 as short}]}});
var breadcrumbPouchEmpty = <breadcrumbtrail:breadcrumb_pouch>.withTag({OpenBag: 0 as byte, BagContents: {Size: 8, Items: []}});

recipes.addShapeless( breadcrumbPouchFull, [<breadcrumbtrail:breadcrumb>.transformConsume(63) *64, breadcrumbPouchEmpty] );
recipes.addShapeless( glowingBreadcrumbPouchFull, [<breadcrumbtrail:breadcrumb_glowing>.transformConsume(63) *64, breadcrumbPouchEmpty] );