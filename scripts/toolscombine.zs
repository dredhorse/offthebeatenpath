import mods.artisanworktables.builder.RecipeBuilder;
import mods.artisanworktables.builder.Copy;
import crafttweaker.item.IItemStack;

// Blacksmith Worktable

static BlacksmithRecipe as RecipeBuilder = RecipeBuilder.get("blacksmith");


// fixes for toolscombine missing recipes



// Iron Tools

var usage = 10;
var lavaAmount = 100;

BlacksmithRecipe
    .setShapeless([<tc:essence>,<minecraft:iron_ingot>,<minecraft:iron_ingot>,<minecraft:iron_ingot>,<minecraft:iron_ingot>,<minecraft:iron_ingot>])
    .addTool(<ore:artisansHammer>, usage)
    .setFluid(<liquid:lava> * lavaAmount)
    .addOutput(<tc:ipaxe>)
    .create();
    recipes.removeByRecipeName("tc:ipaxe");

BlacksmithRecipe
    .setShapeless([<tc:essence>,<minecraft:iron_ingot>,<minecraft:iron_ingot>,<minecraft:iron_ingot>,<minecraft:iron_ingot>])
    .addTool(<ore:artisansHammer>, usage)
    .setFluid(<liquid:lava> * lavaAmount)
    .addOutput(<tc:isaxe>)
    .create();
    recipes.removeByRecipeName("tc:isaxe");



// Diamond Tools

usage = 50;
lavaAmount = 250;

BlacksmithRecipe
    .setShapeless([<tc:essence>,<minecraft:diamond>,<minecraft:diamond>,<minecraft:diamond>,<minecraft:diamond>,<minecraft:diamond>])
    .addTool(<ore:artisansHammer>, usage)
    .setFluid(<liquid:lava> * lavaAmount)
    .addOutput(<tc:dpaxe>)
    .create();
    recipes.removeByRecipeName("tc:dpaxe");

BlacksmithRecipe
    .setShapeless([<tc:essence>,<minecraft:diamond>,<minecraft:diamond>,<minecraft:diamond>,<minecraft:diamond>])
    .addTool(<ore:artisansHammer>, usage)
    .setFluid(<liquid:lava> * lavaAmount)
    .addOutput(<tc:dsaxe>)
    .create();
    recipes.removeByRecipeName("tc:dsaxe");
