import mods.artisanworktables.builder.RecipeBuilder;
import mods.artisanworktables.builder.Copy;
import crafttweaker.item.IItemStack;

// Blacksmith Worktable

static BlacksmithRecipe as RecipeBuilder = RecipeBuilder.get("blacksmith");


// managing Tools


var Tools = [
	"shovel",
	"pickaxe",
	"axe",
	"sword",
	"hoe"
    
] as string[];

var artisanWorktableTools = [
	"artisans_hammer",
	"artisans_mortar",
	"artisans_pliers"
    
] as string[];

var tinyProgressionTools = [
	"battle",
	"spear",
	"multi"
    
] as string[];

var tinyProgressionNetherTools = [
	"axe",
	"sword",
	"pickaxe",
	"hoe",
	"spade"
    
] as string[];



// Iron Tools

var usage = 10;
var lavaAmount = 100;

createRecipies(createMinecraftTools(Tools, "iron"), usage, lavaAmount);
createRecipe("animus:kama_iron", usage, lavaAmount);
createRecipe("roots:iron_knife", usage, lavaAmount);
createRecipe("roots:iron_hammer", usage, lavaAmount);
createRecipies(createTinyProgressionNetherTools(tinyProgressionNetherTools, "iron"), usage, lavaAmount);
createRecipies(createTinyProgressionTools(tinyProgressionTools, "iron"), usage, lavaAmount);
createRecipe("tc:ipade", usage, lavaAmount);
createRecipe("tc:ipaxe", usage, lavaAmount);
createRecipe("tc:isaxe", usage, lavaAmount);
createRecipies(createArtisanWorktableTools(artisanWorktableTools, "iron"), usage, lavaAmount);
createRecipe("minecraft:bucket", usage, lavaAmount);


// Diamond Tools

usage = 50;
lavaAmount = 250;

createRecipies(createMinecraftTools(Tools, "diamond"), usage, lavaAmount);
createRecipe("animus:kama_diamond", usage, lavaAmount);
createRecipe("roots:diamond_knife", usage, lavaAmount);
createRecipe("roots:diamond_hammer", usage, lavaAmount);
createRecipies(createTinyProgressionNetherTools(tinyProgressionNetherTools, "diamond"), usage, lavaAmount);
createRecipies(createTinyProgressionTools(tinyProgressionTools, "diamond"), usage, lavaAmount);
createRecipe("tc:dpade", usage, lavaAmount);
createRecipe("tc:dpaxe", usage, lavaAmount);
createRecipe("tc:dsaxe", usage, lavaAmount);
createRecipies(createArtisanWorktableTools(artisanWorktableTools, "diamond"), usage, lavaAmount);


// Gold Tools

createRecipies(createMinecraftTools(Tools, "gold"), usage, lavaAmount);
createRecipe("animus:kama_gold", usage, lavaAmount);
createRecipe("roots:gold_knife", usage, lavaAmount);
createRecipe("roots:gold_hammer", usage, lavaAmount);
createRecipies(createTinyProgressionNetherTools(tinyProgressionNetherTools, "gold"), usage, lavaAmount);
createRecipies(createTinyProgressionTools(tinyProgressionTools, "gold"), usage, lavaAmount);
createRecipe("tc:gpade", usage, lavaAmount);
createRecipe("tc:gpaxe", usage, lavaAmount);
createRecipe("tc:gsaxe", usage, lavaAmount);
createRecipies(createArtisanWorktableTools(artisanWorktableTools, "gold"), usage, lavaAmount);
createRecipe("nex:golden_witherbone_sword", usage, lavaAmount);
createRecipe("nex:golden_witherbone_pickaxe", usage, lavaAmount);
createRecipe("nex:golden_witherbone_shovel", usage, lavaAmount);
createRecipe("nex:golden_witherbone_axe", usage, lavaAmount);
createRecipe("nex:golden_witherbone_hoe", usage, lavaAmount);
createRecipe("nex:golden_witherbone_hammer", usage, lavaAmount);

// Copper Tools

createRecipies(createArtisanWorktableTools(artisanWorktableTools, "copper"), usage, lavaAmount);
createRecipies(createEmbersTools(Tools, "copper"), usage, lavaAmount);

// Silver Tools

createRecipies(createArtisanWorktableTools(artisanWorktableTools, "silver"), usage, lavaAmount);
createRecipies(createEmbersTools(Tools, "silver"), usage, lavaAmount);

// Lead Tools

createRecipies(createArtisanWorktableTools(artisanWorktableTools, "lead"), usage, lavaAmount);
createRecipies(createEmbersTools(Tools, "lead"), usage, lavaAmount);

// Dawnstone Tools

createRecipies(createEmbersTools(Tools, "dawnstone"), usage, lavaAmount);

// Aluminum Tools

createRecipies(createArtisanWorktableTools(artisanWorktableTools, "aluminum"), usage, lavaAmount);
createRecipies(createEmbersTools(Tools, "aluminum"), usage, lavaAmount);

// Bronze Tools

createRecipies(createArtisanWorktableTools(artisanWorktableTools, "bronze"), usage, lavaAmount);
createRecipies(createEmbersTools(Tools, "bronze"), usage, lavaAmount);

// Electrum Tools

createRecipies(createArtisanWorktableTools(artisanWorktableTools, "electrum"), usage, lavaAmount);
createRecipies(createEmbersTools(Tools, "electrum"), usage, lavaAmount);

// Nickel Tools

createRecipies(createArtisanWorktableTools(artisanWorktableTools, "nickel"), usage, lavaAmount);
createRecipies(createEmbersTools(Tools, "nickel"), usage, lavaAmount);

// Tin Tools

createRecipies(createArtisanWorktableTools(artisanWorktableTools, "tin"), usage, lavaAmount);
createRecipies(createEmbersTools(Tools, "tin"), usage, lavaAmount);

// Obsidian Tools

createRecipies(createTinyProgressionTools(tinyProgressionTools, "obsidian"), usage, lavaAmount);
createRecipies(createTinyProgressionTools(Tools, "obsidian"), usage, lavaAmount);


// Wub Tools

usage = 500;
lavaAmount = 1000;

createRecipies(createTinyProgressionTools(tinyProgressionTools, "wub"), usage, lavaAmount);
createRecipies(createTinyProgressionTools(Tools, "wub"), usage, lavaAmount);
createRecipe("tp:wub_paxel", usage, lavaAmount);
createRecipe("tp:wub_hammer", usage, lavaAmount);

// Nether Tools

usage = 50;
lavaAmount = 250;

createRecipe("tp:nether_sword", usage, lavaAmount);

// Emerald Tools

createRecipies(createTinyProgressionTools(tinyProgressionTools, "emerald"), usage, lavaAmount);
createRecipies(createTinyProgressionTools(Tools, "emerald"), usage, lavaAmount);


// Constantan Tools

createRecipies(createArtisanWorktableTools(artisanWorktableTools, "constantan"), usage, lavaAmount);

// Invar Tools

createRecipies(createArtisanWorktableTools(artisanWorktableTools, "invar"), usage, lavaAmount);

// Platinum Tools

createRecipies(createArtisanWorktableTools(artisanWorktableTools, "platinum"), usage, lavaAmount);

// Steel Tools

createRecipies(createArtisanWorktableTools(artisanWorktableTools, "steel"), usage, lavaAmount);

// End Reborn Tools

usage = 50;
lavaAmount =250;

createRecipies(createEndRebornTools(Tools, "endorium"), usage, lavaAmount);
createRecipies(createEndRebornTools(Tools, "wolframium"), usage, lavaAmount);

// ############################################## Functions


function createEmbersTools(tools  as string[], metal as string) as string[] {
	var embers = [] as string[];
	for tool in tools
	{
	  embers+= "embers:ingot" + metal + "_" + tool;
	}
	return embers;
}	

function createArtisanWorktableTools(tools  as string[], metal as string) as string[] {
	var artisan = [] as string[];
	for tool in tools
	{
	  artisan+= "artisanworktables:recipe." + tool + "." + metal;
	}
	return artisan;
}

function createMinecraftTools(tools as string[], metal as string) as string[] {
	var minecraft = [] as string[];
	for tool in tools
	{
	  minecraft+= "minecraft:" + metal + "_" + tool;
	}
	return minecraft;
}

function createTinyProgressionTools(tools as string[], metal as string) as string[] {
	var tp = [] as string[];
	for tool in tools
	{
	  tp+= "tp:" + metal + "_" + tool;
	}
	return tp;
}

function createTinyProgressionNetherTools(tools as string[], metal as string) as string[] {
	var tpn = [] as string[];
	for tool in tools
	{
	  tpn+= "tp:nether_" + metal + "_" + tool;
	}
	return tpn;
}

function createEndRebornTools(tools as string[], metal as string) as string[] {
	var endReborn = [] as string[];
	for tool in tools
	{
	  endReborn+= "endreborn:tools/tool_" + tool + "_" + metal;
	}
	return endReborn;
}

function createRecipe(item as string, usage as int, lavaAmount as int) {
   BlacksmithRecipe
   .setCopy(
    	Copy.byName(item)
    )
    .addTool(<ore:artisansHammer>, usage)
    .setFluid(<liquid:lava> * lavaAmount)
    .create();
    recipes.removeByRecipeName(item);

}

function createRecipies(items as string[], usage as int, lavaAmount as int){
	for item in items {
		createRecipe(item, usage, lavaAmount);
	}
}