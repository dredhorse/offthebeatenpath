import mods.artisanworktables.builder.RecipeBuilder;
import mods.artisanworktables.builder.Copy;
import crafttweaker.item.IItemStack;

// Engineer Worktable

val EngineerRecipe = RecipeBuilder.get("engineer");

// making Fluid Pipes harder

EngineerRecipe.setCopy(
    	Copy.byName("cd4017be_lib:indlog/fluid_pipe_0")
    )
    .addTool(<ore:artisansPliers>, 10)
	.setFluid(<liquid:vasholine> * 100)
    .create();
recipes.removeByRecipeName("cd4017be_lib:indlog/fluid_pipe_0");

// adding Artisans Toolboxes

EngineerRecipe.setShapeless([
    <ore:chest>])
  .addTool(<ore:artisansHammer>,10)
  .addOutput(<artisanworktables:toolbox>)
  .create();
  
EngineerRecipe.setShapeless([
    <artisanworktables:toolbox>,<embers:mech_core> ])
  .addTool(<ore:artisansHammer>,50)
  .setFluid(<liquid:vasholine> * 100)
  .addOutput(<artisanworktables:mechanical_toolbox>)
  .create();
  
  
EngineerRecipe.setShapeless([
    <artisanworktables:worktable:1>,<architecturecraft:sawbench> ])
  .addTool(<ore:artisansHammer>,50)
  .setFluid(<liquid:vasholine> * 100)
  .addOutput(<artisanworktables:workstation:1>)
  .create();
  


// making The One Probe harder

EngineerRecipe.setCopy(
    	Copy.byName("theoneprobe:probe")
    )
    .addTool(<ore:artisansPliers>, 500)
	.setFluid(<liquid:vasholine> * 100)
    .create();
recipes.removeByRecipeName("theoneprobe:probe");