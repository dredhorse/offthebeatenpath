import crafttweaker.item.IItemStack;
import crafttweaker.item.IIngredient;
import mods.artisanworktables.builder.RecipeBuilder;
import mods.artisanworktables.builder.Copy;

// Mage Worktable

static MageRecipe as RecipeBuilder = RecipeBuilder.get("mage");

// Arche Stones

static lMinium as IItemStack = <arche:lminiumstone>;
static miniumStone as IItemStack = <arche:miniumstone>;
static phiStone as IItemStack = <arche:phistone>;

// Variables

var stone = <minecraft:stone:0>;
var cobble = <minecraft:cobblestone>;
var sand = <minecraft:sand:0>;
var gravel = <minecraft:gravel>;
var endStone = <minecraft:end_stone>;
var granite = <minecraft:stone:1>;
var diorite = <minecraft:stone:3>;
var andesite = <minecraft:stone:5>;
var glowstone = <minecraft:glowstone_dust>;
var gunpowder = <minecraft:gunpowder>;
var torch = <minecraft:torch>;
var slime = <minecraft:slime_ball>;
var ricecake = <harvestcraft:ricecakeitem>;
var waterbucket = <minecraft:water_bucket>;
var ink = <minecraft:dye:0>;
var obsidian = <minecraft:obsidian>;
var feather = <minecraft:feather>;
var minestring = <minecraft:string>;



// All Tier Recipes

addToAllTier( [stone], cobble);
addToAllTier( [cobble], gravel);
addToAllTier( [gravel], sand);
addToAllTier( [gunpowder, gunpowder], glowstone);
addToAllTier( [torch], glowstone);
addToAllTier( [ricecake,waterbucket],slime);
addToAllTier( [obsidian], ink * 4);
addToAllTier( [minestring, minestring,minestring,minestring], feather);


// Second Tier Recipes

addToTier2( [sand], gravel);
addToTier2( [gravel], cobble);
addToTier2( [cobble], stone);
addToTier2( [granite], diorite);
addToTier2([diorite], andesite);


// Philosopher Stone Recipes

addToPhiStone( [stone], endStone);
addToPhiStone([sand], stone);
addToPhiStone([andesite], granite);


// ############################################## Functions


function addToAllTier(input as IIngredient[], output as IItemStack) {
	addRecipe( input, output, lMinium);
	addRecipe( input, output, miniumStone);
	addRecipe( input, output, phiStone); 

}	

function addToTier2(input as IIngredient[], output as IItemStack) {

	addRecipe( input, output, miniumStone);
	addRecipe( input, output, phiStone); 
}

function addToPhiStone(input as IIngredient[], output as IItemStack) {
	
	addRecipe( input, output, phiStone); 
}

function addRecipe(input as IIngredient[], output as IItemStack, stone as IItemStack ) {

MageRecipe
  .setShapeless(input)
  .addTool(stone,1)
  .addOutput(output)
  .create();

}

