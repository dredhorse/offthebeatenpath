import mods.artisanworktables.builder.RecipeBuilder;
import mods.artisanworktables.builder.Copy;
import crafttweaker.item.IItemStack;

// Mage Worktable

val MageRecipe = RecipeBuilder.get("mage");

// change salis mundus
<thaumcraft:salis_mundus>.addTooltip(format.red("Use the Artisan Table Recipe"));
MageRecipe
  .setShaped([
    [<ore:visCrystal>, <ore:visCrystal>, null],[<ore:visCrystal>, <ore:dustRedstone>, null], [null, null, null]])
  .addTool(<ore:artisansMortar>,10)
  .addOutput(<thaumcraft:salis_mundus>)
  .create();


// lock Thaumcraft Infusion behind BloodMagic and Artisan Worktables
recipes.removeByRecipeName("thaumcraft:stonearcane");
MageRecipe
  .setShaped([
    [<bloodmagic:ritual_controller>, <bloodmagic:ritual_controller>, <bloodmagic:ritual_controller>], [<bloodmagic:ritual_controller>, <ore:visCrystal>, <bloodmagic:ritual_controller>], [<bloodmagic:ritual_controller>, <bloodmagic:ritual_controller>, <bloodmagic:ritual_controller>]])
  .addTool(<ore:artisansHammer>,10)
  .addOutput(<thaumcraft:stone_arcane> * 9)
  .create();


// make Waystone Warp Stone harder (possible move to gem cutter table?)

recipes.removeByRecipeName("waystones:warp_stone");
MageRecipe
  .setShaped([
    [<thaumcraft:nitor_purple>, <ore:enderpearl>, <thaumcraft:nitor_purple>], [<ore:enderpearl>, <bloodmagic:teleposition_focus:1>, <ore:enderpearl>], [<thaumcraft:nitor_purple>, <ore:enderpearl>, <thaumcraft:nitor_purple>]])
  .addTool(<ore:artisansHammer>,10)
  .addOutput(<waystones:warp_stone>)
  .create();


// add iron ore recipes

MageRecipe
  .setShaped([
    [<bloodmagic:component:19>, <bloodmagic:component:19>, <bloodmagic:component:19>],[<bloodmagic:component:19>, <minecraft:concrete_powder:8>, <bloodmagic:component:19>], [<bloodmagic:component:19>, <bloodmagic:component:19>, <bloodmagic:component:19>]])
  .addTool(<ore:artisansMortar>,100)
  .setFluid(<liquid:water> * 1000)
  .addOutput(<minecraft:iron_ore>)
  .create();

// make magic feather easier (move to begin of playing with the help of the questbook)

recipes.removeByRecipeName("magicfeather:magicfeather");
MageRecipe
  .setShaped([
    [<minecraft:gold_ingot>, <minecraft:feather>, <minecraft:gold_ingot>], [<minecraft:gold_ingot>, <flying_things:phial_of_animation>, <minecraft:gold_ingot>], [<minecraft:gold_ingot>, <minecraft:feather>, <minecraft:gold_ingot>]])
  .addTool(<ore:artisansMortar>,100)
  .setFluid(<liquid:water> * 1000)
  .addOutput(<magicfeather:magicfeather>)
  .create(); 