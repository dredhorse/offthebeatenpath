#priority 10000

import crafttweaker.item.IIngredient;
import crafttweaker.item.IItemStack;

// Remove Recipes By Name

val RemRecByName = [
    "thaumcraft:cinderpearltoblazepowder",
    "projecte:philosophers_stone_alt",
    "projecte:philosophers_stone",
    "projecte:collector_mk1",
    "projecte:condenser_mk1",
    "arche:phis/vanilla/leatheritem",
    "arche:minium/vanilla/leatheritem",
    "arche:lminium/vanilla/leatheritem",
    "dooglamoojuniorarchaeologymod:ink",
    "dooglamoojuniorarchaeologymod:leather2",
    "thaumcraft:salismundus",
    "naturescompass:natures_compass",
    "refinedstorage:controller",
    "endercompass:ender_compass",
    "oresalleasy:items/bucket",
    "harvestcraft:well",
    "arche:phis/vanilla/redstoneitem",
    "arche:minium/vanilla/redstoneitem",
    "arche:lminium/vanilla/redstoneitem",
    "tp:watering_can",
    "furnus:upgrade_5_1_9541",
    "minecraft:brewing_stand",
    "tp:quartz_ingot",
    "harvestcraft:market"
    
    
] as string[];


for item in RemRecByName{
    recipes.removeByRecipeName(item);
}


