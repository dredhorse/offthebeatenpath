// Remove and Hide
import crafttweaker.item.IItemStack;


var RemHide = [
    <refinedstorage:machine_casing>,
    <meecreeps:portalgun>,
    <meecreeps:cartridge>,
    <meecreeps:emptyportalgun>,
    <meecreeps:projectile>,
    <meecreeps:portalblock>,
    <harvestcraft:groundtrap>,
    <harvestcraft:market>
    
    
] as IItemStack[];


for item in RemHide{
    mods.jei.JEI.removeAndHide(item, false);
}

