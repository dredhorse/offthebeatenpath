// locking Refined Storage behind Embers and Astral Sorcery

recipes.replaceAllOccurences(<refinedstorage:machine_casing>, <embers:crystal_cell>);

mods.astralsorcery.Altar.addAttunementAltarRecipe("OffTheBeatenPath:refinedstorage/controller", <refinedstorage:controller>, 500, 300, [
            <refinedstorage:quartz_enriched_iron>, <refinedstorage:processor:5>, <refinedstorage:quartz_enriched_iron>,
            <refinedstorage:silicon>, <embers:crystal_cell>, <refinedstorage:silicon>,
            <refinedstorage:quartz_enriched_iron>, <refinedstorage:silicon>, <refinedstorage:quartz_enriched_iron>,
            <astralsorcery:itemshiftingstar>, <astralsorcery:itemshiftingstar>, <astralsorcery:itemshiftingstar>, <astralsorcery:itemshiftingstar>]);


// locking Nature and Ender Compass behind BloodMagic

recipes.addShaped(<naturescompass:naturescompass>, [[<ore:treeSapling>, <ore:logWood>, <ore:treeSapling>], [<ore:logWood>, <bloodmagic:sigil_seer>, <ore:logWood>], [<ore:treeSapling>, <ore:logWood>, <ore:treeSapling>]]);
recipes.addShaped(<endercompass:ender_compass>, [[null, <minecraft:ender_eye>, null], [<minecraft:ender_eye>, <bloodmagic:sigil_seer>, <minecraft:ender_eye>], [null, <minecraft:ender_eye>, null]]);


// locking some natural stuff behind Nature's Aura
recipes.addShaped(<tp:watering_can>, [[<naturesaura:infused_iron>, <minecraft:dye:15>, null], [<naturesaura:infused_iron>, <minecraft:water_bucket>, <naturesaura:infused_iron>], [null, <naturesaura:infused_iron>, null]]);
recipes.addShaped(<furnus:upgrade:5>, [[null, <minecraft:coal>, null], [<naturesaura:infused_iron>,<minecraft:iron_ingot>, <naturesaura:infused_iron>], [null, <minecraft:coal_block>, null]]);
recipes.addShaped(<minecraft:brewing_stand>, [[null, null, null], [null, <naturesaura:infused_iron>, null], [<minecraft:cobblestone>, <minecraft:cobblestone>, <minecraft:cobblestone>]]);








